import React from "react";
import {
    Col,
    Form,
    Button,
    FormInput,
    FormGroup,
    FormFeedback
  } from "shards-react";

import {
	withRouter
} from 'react-router-dom';
import UserStore from '../../MobxStore/UserStore';



class Login extends React.Component {
    constructor(props){
        super(props);
        this.state={
            username:'',
            password:'',
            unvalid: false,
            interneterror: false,
            data: []
        }
    }
    handleUsertname(text){
    this.setState({username: text.target.value})
    }
    handlePassword(text){
    this.setState({password: text.target.value})
    }
    handleSubmit = async (event) =>{
        event.preventDefault();
        const user = {
            username: this.state.username,
            password: this.state.password
        };
        const loginResponse = await UserStore.login(user)
        if(loginResponse.status == 401){
            this.setState({unvalid: true})
            this.setState({interneterror: false})
        }else if(loginResponse.status == 200){
            if(loginResponse.role !== "admin"){
                this.props.history.push('/home');
            }else{
                this.props.history.push('/blog-overview');
            }
            alert("login success")
        }else{
            alert("Internet Error. Please check your connection")
        }
        // axios.post('http://localhost:3000/users/login', user)
        // .then(user => {
        //     console.log(user.data.token)
        //     console.log(user.status)
        //     if(user.status === 200){
        //     console.log("Login success")
        //     console.log(user)
        //     localStorage.setItem("cool-jwt", user.data.token);
        //     if(user.role !== "admin")
        //     {
        //         this.props.history.push('/home');
        //     }else{
        //         this.props.history.push('/blog-overview');
        //     }
        //     }
        // })
        // .catch((error) => {
        //     if(error.response.status === 401){
        //         this.setState({unvalid: true})
        //         this.setState({interneterror: false})
        //     }else{
        //         this.setState({interneterror: true})
        //         this.setState({unvalid: false})
        //     }
        // })
    }
    render() {
      return (
          <div className="login-page">
              <div className="row mt-3">
                <div className="col-xl-7 col-lg-8 col-md-8 col-sm-8 m-auto">
                    <div className="col-xl-10 col-lg-12 col-md-12 mb-4 pt-3 card card-small">
                        <Col sm="12" md="12">
                            <h3 className="text-muted d-block mb-2">LOGIN</h3><br/>
                            { this.state.unvalid 
                                ? ( 
                                    <FormFeedback>Incorrect username or password</FormFeedback>
                                )
                                : null
                            }
                            { this.state.interneterror 
                                ? ( 
                                    <FormFeedback>Internet error</FormFeedback>
                                )
                                : null
                            }
                            <Form onSubmit={this.handleSubmit}>
                                <a className="text-muted d-block mb-2">User Name </a>
                                <FormGroup>
                                    <FormInput
                                    placeholder="User name"
                                    //value=""
                                    onChange={(text) => {this.handleUsertname(text)}}
                                    />
                                </FormGroup> 
                                <FormGroup>
                                    <FormInput
                                    type="password"
                                    placeholder="Password"
                                    //value="myCoolPassword"
                                    onChange={(text) => {this.handlePassword(text)}}
                                    />
                                </FormGroup>
                                <a className="text-muted d-block mb-2 forget" href="#">Forget password?</a>
                                <Button theme="primary" className="mb-2 mr-1">
                                    Login
                                </Button><br/>
                                <p className="text-muted d-block mb-2 signin">Don't have an account? <a href="/signup">Sign up</a></p>
                            
                            </Form>
                        </Col>
                    </div>
                </div>
            </div>
          </div>
      )
    }
}
export default withRouter(Login);

