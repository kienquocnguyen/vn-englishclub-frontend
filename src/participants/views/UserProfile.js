import React from "react";
import axios from 'axios';
import { 
        Container, 
        Row,
        Card,
        Button,
        ListGroup,
        CardHeader,
        ListGroupItem,
        Form,
        FormGroup,
        FormInput, 
        Col 
    } from "shards-react";

import PageTitle from "../.././admin/components/common/PageTitle";
import UserStore from "../../MobxStore/UserStore";
import { observer } from 'mobx-react';

@observer
class UserProfile extends React.Component {
    constructor(props){
        super(props);
        this.state={
            users: [{
                firstName: '',
                lastName: '',
                facebook: '',
                email: '',
                phoneNumber: '',
                city: '',
                address: '',
                district: '',
            }]
        }
    }
    
    handleFirstname(text){
        let newFirstName = Object.assign({}, this.state);
        newFirstName.users.firstName = text.target.value;
        this.setState({newFirstName})
    }
    handleLastname(text){
        let newLastName = Object.assign({}, this.state);
        newLastName.users.lastName = text.target.value;
        this.setState({newLastName})
    }
    handleFacebook(text){
        let newFacebook = Object.assign({}, this.state);
        newFacebook.users.facebook = text.target.value;
        this.setState({newFacebook})
    }
    handleEmail(text){
        let newEmail = Object.assign({}, this.state);
        newEmail.users.email = text.target.value;
        this.setState({newEmail})
    }
    handlePhonenumber(text){
        let newPhonenumber = Object.assign({}, this.state);
        newPhonenumber.users.phoneNumber = text.target.value;
        this.setState({newPhonenumber})
    }
    handleAddress(text){
        let newAddress = Object.assign({}, this.state);
        newAddress.users.address = text.target.value;
        this.setState({newAddress})
    }
    handleDistrict(text){
        let newDistrict = Object.assign({}, this.state);
        newDistrict.users.district = text.target.value;
        this.setState({newDistrict})
    }
    handleWard(text){
        let newWard = Object.assign({}, this.state);
        newWard.users.ward = text.target.value;
        this.setState({newWard})
    }
    handleCity(text){
        let newCity = Object.assign({}, this.state);
        newCity.users.city = text.target.value;
        this.setState({newCity})
    }
    handleUpdateUser = async (event) =>{
        event.preventDefault();
        const userinfo = this.state.users
        const user = {
          firstName: userinfo.firstName,
          lastName: userinfo.lastName,
          facebook: userinfo.facebook,
          email: userinfo.email,
          phoneNumber: userinfo.phoneNumber,
          city: userinfo.city,
          address: userinfo.address,
          district: userinfo.district,
          ward: userinfo.ward,     
        };

        const updateResponse = await UserStore.userprofile(user)
        if(updateResponse.status == 401){
            this.setState({unvalid: true})
            this.setState({interneterror: false})
        }else if(updateResponse.status == 200){
            if(updateResponse.role !== "admin"){
                this.props.history.push('/userprofile');
            }else{
                this.props.history.push('/blog-overview');
            }
            alert("update success")
        }else{
            alert("Internet Error. Please check your connection")
        }
    }
    componentDidMount =  async () => {
        const loggedinResponse = await UserStore.logedin();
        if(loggedinResponse){
            return;
        }else{
            this.props.history.push('/home');
        }
    }
      
    
    render() {
        const user = UserStore.userInfo;
        return (
            <div className="user-profile">
                <Container fluid className="main-content-container px-4">
                        <Row noGutters className="page-header py-4">
                        <PageTitle title="User Profile" subtitle="Overview" md="12" className="ml-sm-auto mr-sm-auto" />
                        </Row>
                        <Row>
                        <Col lg="4">
                            <Card small className="mb-4 pt-3">
                                <CardHeader className="border-bottom text-center">
                                <div className="mb-3 mx-auto">
                                    <img
                                        className="rounded-circle"
                                        src={user && user.avatar ? UserStore.BASE_PHOTO_API + user.avatar : require("../../admin/images/avatars/0.jpg")}
                                        alt={user && user.lastName ? user.lastName : ''}
                                        width="110"
                                    />
                                </div>
                                <h4 className="mb-0 user-full-name">{user && user.firstName ? user.firstName : ''} {user && user.lastName ? user.lastName : ''}</h4>
                                <h5 className="mb-0 user-role">{user && user.role ? user.role : ''}</h5>
                                <Button pill outline size="sm" className="mb-2">
                                    <i className="material-icons mr-1">event</i> Check Calender
                                </Button>
                                </CardHeader>
                            </Card>
                        </Col>
                        <Col lg="8">
                            <Card small className="mb-4">
                                <CardHeader className="border-bottom">
                                <h6 className="m-0">Account Details</h6>
                                </CardHeader>
                                <ListGroup flush>
                                <ListGroupItem className="p-3">
                                    <Row>
                                    <Col>
                                        <Form>
                                        <Row form>
                                            {/* First Name */}
                                            <Col md="6" className="form-group">
                                            <label htmlFor="feFirstName">First Name</label>
                                            <FormInput
                                                id="feFirstName"
                                                placeholder="First Name"
                                                defaultValue={user && user.firstName ? user.firstName : ''}
                                                onChange={(text) => {this.handleFirstname(text)}}
                                            />
                                            </Col>
                                            {/* Last Name */}
                                            <Col md="6" className="form-group">
                                            <label htmlFor="feLastName">Last Name</label>
                                            <FormInput
                                                id="feLastName"
                                                placeholder="Last Name"
                                                value={user && user.lastName ? user.lastName : ''}
                                                onChange={(text) => {this.handleLastname(text)}}
                                            />
                                            </Col>
                                        </Row>
                                        <Row form>
                                            {/* Email */}
                                            <Col md="6" className="form-group">
                                            <label htmlFor="feEmail">Email</label>
                                            <FormInput
                                                type="email"
                                                id="feEmail"
                                                placeholder="Email Address"
                                                value={user && user.email ? user.email : ''}
                                                onChange={(text) => {this.handleEmail(text)}}
                                                autoComplete=""
                                            />
                                            </Col>
                                            {/* Phone Number */}
                                            <Col md="6" className="form-group">
                                            <label htmlFor="fePassword">Phone Number</label>
                                            <FormInput
                                                type="text"
                                                id="fePassword"
                                                placeholder=""
                                                value={user && user.phoneNumber ? user.phoneNumber: ''}
                                                onChange={(text) => {this.handlePhonenumber(text)}}
                                                autoComplete=""
                                            />
                                            </Col>
                                        </Row>
                                        <Row form>
                                            
                                            {/* Password */}
                                            <Col md="6" className="form-group">
                                            <label htmlFor="fePassword">User Name</label><br/>
                                            <label htmlFor="fePassword">{user && user.username ? user.username : ''}</label>
                                            </Col>
                                            {/* Face Book */}
                                            <Col md="6" className="form-group">
                                            <label htmlFor="feEmail">Facebook</label>
                                            <FormInput
                                                type="text"
                                                id="feEmail"
                                                placeholder="Facebook Address"
                                                value={user && user.facebook ? user.facebook : ''}
                                                onChange={(text) => {this.handleFacebook(text)}}
                                                autoComplete="email"
                                            />
                                            </Col>
                                        </Row>
                                        <FormGroup>
                                            <label htmlFor="feAddress">Address</label>
                                            <FormInput
                                            id="feAddress"
                                            placeholder="Address"
                                            value={user && user.address ? user.address : ''}
                                            onChange={(text) => {this.handleAddress(text)}}
                                            />
                                        </FormGroup>
                                        <Row form>
                                            {/* City */}
                                            <Col md="4" className="form-group">
                                            <label htmlFor="feCity">City</label>
                                            <FormInput
                                                id="feCity"
                                                placeholder="City"
                                                value={user && user.city ? user.city : ''}
                                                onChange={(text) => {this.handleCity(text)}}
                                            />
                                            </Col>
                                            {/* District */}
                                            <Col md="3" className="form-group">
                                            <label htmlFor="feInputState">District</label>
                                            <FormInput
                                                id="feCity"
                                                placeholder="City"
                                                value={user && user.district ? user.district : ''}
                                                onChange={(text) => {this.handleDistrict(text)}}
                                            />
                                            </Col>
                                            {/* Ward */}
                                            <Col md="3" className="form-group">
                                            <label htmlFor="feInputState">Ward</label>
                                            <FormInput
                                                id="feCity"
                                                placeholder="City"
                                                value={user && user.ward ? user.ward : ''}
                                                onChange={(text) => {this.handleWard(text)}}
                                            />
                                            </Col>
                                            {/* Zip Code */}
                                            <Col md="2" className="form-group">
                                            <label htmlFor="feZipCode">Zip</label>
                                            <FormInput
                                                id="feZipCode"
                                                placeholder="Zip"
                                                onChange={() => {}}
                                            />
                                            </Col>
                                        </Row>
                                        
                                        <Button theme="accent" className="btn-update" onClick={this.handleUpdateUser}>Update Account</Button>
                                        <Button theme="accent" className="btn-change-pass">Change Password</Button>
                                        </Form>
                                    </Col>
                                    </Row>
                                </ListGroupItem>
                                </ListGroup>
                            </Card>
                        </Col>
                        </Row>
                    </Container>
            </div>
        )
    }
}
export default UserProfile;