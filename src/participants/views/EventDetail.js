import React from "react";
import {
    Col,
    Row,
    Card,
    CardBody,
    FormInput,
    Form,
    CardHeader,
    Button,
    Badge
  } from "shards-react";

import {
	withRouter
} from 'react-router-dom';
import CalendarIcon from "react-calendar-icon";
import UserStore from '../../MobxStore/UserStore';
import CommentInput from '../components/CommentInput';
import { observer } from "mobx-react";
import moment from "moment";
import ReactHtmlParser from 'react-html-parser';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from 'react-loader-spinner';
import FbImageLibrary from 'react-fb-image-grid'

const dateOptions = {
  footer: { day: "2-digit" },
  value: { month: "short" },
  backgroundColor: "#f2f6f9",
  textColor: "#3f3f3f",
  locale: "eng"
};
let commentPageLimt = 3;
@observer
class EventDetail extends React.Component {
    constructor(props){
        super(props);
        this.state={
            startDate: '',
            endDate: '',
            commentContent: '',
            loading: true,
            loadingComments: true,
            openEmoji: false,
            userData: false,
            checkLiked: false,
            comments: []
        }
        this.totalLike = 0;
    }

    componentDidMount = async () =>{
        this.setState({loading: true});
        this.setState({loadingComments: true});
        if(!this.props.match.params.permalink){
            this.props.history.push('/home');
        }else{
            if(UserStore.userInfo){
                this.setState({userData: true})
            }else{
                this.setState({userData: false})
            }

            await UserStore.getEventDetail(this.props.match.params.permalink);
            this.totalLike = UserStore.singleEvent.totalLike;
            if(UserStore.singleEvent.type == 'weekly'){
                const weekdays = moment(new Date(UserStore.singleEvent.startDate)).format('dddd')
                if(weekdays === 'Monday'){
                    function getMonday(d) {
                        d = new Date(d);
                        if(moment(d).format('dddd') == 'Sunday'){
                            var day = d.getDay(),
                            diff = d.getDate() - day + (day == 0 ? -6:6); // adjust when day is sunday
                        }else{
                            var day = d.getDay(),
                            diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
                        }
                        return new Date(d.setDate(diff));
                    }
                    let unConvertStartDate = getMonday(new Date());
                    let startTime = moment(new Date(UserStore.singleEvent.startDate)).format('HH:mm');
                    let endTime = moment(new Date(UserStore.singleEvent.expiredDate)).format('HH:mm');
                    this.setState({startDate: moment(new Date(unConvertStartDate)).format('dddd, DD/MM/YYYY') + ', ' + startTime})
                    this.setState({endDate: endTime})
                }

                else if(weekdays === 'Tuesday'){
                    function getTuesday(d){
                        d = new Date(d);
                        if(moment(d).format('dddd') == 'Sunday'){
                            var day = d.getDay(),
                            diff = d.getDate() - day + (day == 0 ? -5:6); // adjust when day is sunday
                        }else{
                            var day = d.getDay(),
                            diff = d.getDate() - day + (day == 0 ? -6:2); // adjust when day is sunday
                        }
                        return new Date(d.setDate(diff));
                    }
                    let unConvertStartDate = getTuesday(new Date());
                    let startTime = moment(new Date(UserStore.singleEvent.startDate)).format('HH:mm');
                    let endTime = moment(new Date(UserStore.singleEvent.expiredDate)).format('HH:mm');
                    this.setState({startDate: moment(new Date(unConvertStartDate)).format('dddd, DD/MM/YYYY') + ', ' + startTime})
                    this.setState({endDate: endTime})
                }

                else if(weekdays === 'Wednesday'){
                    function getWednesday(d){
                        d = new Date(  d);
                        if(moment(d).format('dddd') == 'Sunday'){
                            var day = d.getDay(),
                            diff = d.getDate() - day + (day == 0 ? -4:6); // adjust when day is sunday
                        }else{
                            var day = d.getDay(),
                            diff = d.getDate() - day + (day == 0 ? -6:3); // adjust when day is sunday
                        }
                        return new Date(d.setDate(diff));
                    }
                    let unConvertStartDate = getWednesday(new Date());
                    let startTime = moment(new Date(UserStore.singleEvent.startDate)).format('HH:mm');
                    let endTime = moment(new Date(UserStore.singleEvent.expiredDate)).format('HH:mm');
                    this.setState({startDate: moment(new Date(unConvertStartDate)).format('dddd, DD/MM/YYYY') + ', ' + startTime})
                    this.setState({endDate: endTime})
                }

                else if(weekdays === 'Thursday'){
                    function getThursday(d){
                        d = new Date(d);
                        if(moment(d).format('dddd') == 'Sunday'){
                            var day = d.getDay(),
                            diff = d.getDate() - day + (day == 0 ? -3:6); // adjust when day is sunday
                        }else{
                            var day = d.getDay(),
                            diff = d.getDate() - day + (day == 0 ? -6:4); // adjust when day is sunday
                        }
                        return new Date(d.setDate(diff));
                    }
                    let unConvertStartDate = getThursday(new Date());
                    let startTime = moment(new Date(UserStore.singleEvent.startDate)).format('HH:mm');
                    let endTime = moment(new Date(UserStore.singleEvent.expiredDate)).format('HH:mm');
                    this.setState({startDate: moment(new Date(unConvertStartDate)).format('dddd, DD/MM/YYYY') + ', ' + startTime})
                    this.setState({endDate: endTime})
                }

                else if(weekdays === 'Friday'){
                    function getFriday(d){
                        d = new Date(d);
                        if(moment(d).format('dddd') == 'Sunday'){
                            var day = d.getDay(),
                            diff = d.getDate() - day + (day == 0 ? -2:6); // adjust when day is sunday
                        }else{
                            var day = d.getDay(),
                            diff = d.getDate() - day + (day == 0 ? -6:5); // adjust when day is sunday
                        }
                        return new Date(d.setDate(diff));
                    }
                    let unConvertStartDate = getFriday(new Date());
                    let startTime = moment(new Date(UserStore.singleEvent.startDate)).format('HH:mm');
                    let endTime = moment(new Date(UserStore.singleEvent.expiredDate)).format('HH:mm');
                    this.setState({startDate: moment(new Date(unConvertStartDate)).format('dddd, DD/MM/YYYY') + ', ' + startTime})
                    this.setState({endDate: endTime})
                }

                else if(weekdays === 'Saturday'){
                    function getSaturday(d){
                        d = new Date(d);
                        if(moment(d).format('dddd') == 'Sunday'){
                            var day = d.getDay(),
                            diff = d.getDate() - day + (day == 0 ? -1:6); // adjust when day is sunday
                        }else{
                            var day = d.getDay(),
                            diff = d.getDate() - day + (day == 0 ? -6:6); // adjust when day is sunday
                        }
                        return new Date(d.setDate(diff));
                    }
                    let unConvertStartDate = getSaturday(new Date());
                    let startTime = moment(new Date(UserStore.singleEvent.startDate)).format('HH:mm');
                    let endTime = moment(new Date(UserStore.singleEvent.expiredDate)).format('HH:mm');
                    this.setState({startDate: moment(new Date(unConvertStartDate)).format('dddd, DD/MM/YYYY') + ', ' + startTime})
                    this.setState({endDate: endTime})
                }

                else if(weekdays === 'Sunday'){
                    function getSunday(d){
                        d = new Date(d);
                        var day = d.getDay(),
                        diff = d.getDate() - day + (day == 0 ? -6:6); // adjust when day is sunday
                        return new Date(d.setDate(diff));
                    }
                    let unConvertStartDate = getSunday(new Date());
                    let startTime = moment(new Date(UserStore.singleEvent.startDate)).format('HH:mm');
                    let endTime = moment(new Date(UserStore.singleEvent.expiredDate)).format('HH:mm');
                    this.setState({startDate: moment(new Date(unConvertStartDate)).format('dddd, DD/MM/YYYY') + ', ' + startTime})
                    this.setState({endDate: endTime})
                }
            }else if(UserStore.singleEvent.type == 'daily'){
                this.setState({startDate: moment(new Date(UserStore.singleEvent.startDate)).format('HH:mm')});
                this.setState({endDate: moment(new Date(UserStore.singleEvent.expiredDate)).format('HH:mm')});
            }
            // else if for daily
            else{
                this.setState({startDate: moment(new Date(UserStore.singleEvent.startDate)).format('dddd, DD/MM/YYYY, HH:mm')});
                this.setState({endDate: moment(new Date(UserStore.singleEvent.expiredDate)).format('dddd, DD/MM/YYYY, HH:mm')});
            }
        }
        await UserStore.checkLikedEvent(UserStore.singleEvent._id);
        await UserStore.getEventComments(UserStore.singleEvent._id, 1, commentPageLimt);
        this.setState({checkLiked: UserStore.likeEventChecked})
        this.setState({loading: false});
        this.setState({loadingComments: false});
    }

    handleLike = async () =>{
        if(UserStore.likeEventChecked == true){
            this.totalLike = this.totalLike - 1;
            await UserStore.unLikeEvent(UserStore.singleEvent._id, UserStore.userInfo._id);
            await UserStore.checkLikedEvent(UserStore.singleEvent._id);
            this.setState({checkLiked: UserStore.likeEventChecked})
        }else{
            const body = {
                event_id: UserStore.singleEvent._id,
                event_title: UserStore.singleEvent.title
            }
            this.totalLike = this.totalLike + 1;
            await UserStore.likeEvent(body);
            await UserStore.checkLikedEvent(UserStore.singleEvent._id);
            this.setState({checkLiked: UserStore.likeEventChecked})
        }
    }

    showMoreComments = async () =>{
        this.setState({loadingComments: true});
        commentPageLimt = commentPageLimt + 3;
        if(commentPageLimt > UserStore.commentTotal){
            this.setState({loadingComments: false});
            return;
        }else{
            await UserStore.getEventComments(UserStore.singleEvent._id, 1, commentPageLimt);
        }
        this.setState({loadingComments: false});
    }

    render() {
      const eventDetail = UserStore.singleEvent;
      return (
        <div className="event-detail">
            <div className="event-list">
                <Row>
                    <Col lg="12" md="12" sm="12" className="mb-4">
                        <Card small className="card-post card-post--aside card-post--1">
                            {this.state.loading ? (
                                <div className="loading-container">
                                    <Loader
                                        type="Oval"
                                        color="#1d84ef"
                                        height={50}
                                        width={50}
                                    />
                                </div>
                            ) : (
                                <div>
                                    <CardHeader>
                                        <div className="date-container">
                                            <p>{this.state.startDate} - {this.state.endDate}</p>
                                        </div>
                                        <div className="card-post__image" style={{ backgroundImage: `url(${UserStore.BASE_PHOTO_API}${eventDetail.avatar})` }}></div>
                                    </CardHeader>
                                    <CardBody>
                                        <Row>
                                            <Col lg="10" md="12">    
                                                <h5 className="card-title">
                                                    <div className="calendar-icon"
                                                    >
                                                        <CalendarIcon date={new Date(eventDetail.startDate)} options={dateOptions}/>
                                                    </div>
                                                    <div className="event-info">
                                                        <a className="event-title" href="#" >
                                                            {eventDetail.title}
                                                        </a>
                                                        <div className="event-location">
                                                            <h6 className="material-icons">location_on</h6>
                                                            <a href={`http://maps.google.com/?q=${eventDetail.address}, ward ${eventDetail.ward}, district ${eventDetail.district}, ${eventDetail.city} city, ${eventDetail.country}`} target="_blank">
                                                                <span>
                                                                    {eventDetail.address}, ward {eventDetail.ward}, district {eventDetail.district}, {eventDetail.city} city, {eventDetail.country}
                                                                </span>
                                                            </a>
                                                        </div>
                                                        <div className="like-container">
                                                            <Button className="mr-1 like-button" onClick={this.handleLike}>
                                                                <i className={this.state.checkLiked ? "material-icons like-icon": "material-icons"} >{this.state.checkLiked ? "favorite": "favorite_border"}</i>
                                                            </Button>
                                                            <span>{this.totalLike} people like this event.</span>
                                                        </div>
                                                    </div>
                                                </h5>
                                            </Col>
                                            <Col lg="2" md="12"> 
                                                <div className="event-info">
                                                    <div className="event-category">
                                                        <Row>
                                                            <Col lg="6" md="2" >
                                                                <Badge
                                                                    pill
                                                                    className={'card-post__category bg-info ' + eventDetail.type}  
                                                                >
                                                                    {eventDetail.type}
                                                                </Badge>
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                </div>
                                            </Col>
                                        </Row>
                                        <div className="card-text d-inline-block">
                                            {eventDetail.description}
                                        </div>
                                        <div className="mt-3 mb-4">
                                            <FbImageLibrary
                                                hideOverlay={true} 
                                                images={[
                                                    UserStore.BASE_PHOTO_API + "66227451_2372948849694506_6389753168451862528_o.jpg",
                                                    UserStore.BASE_PHOTO_API + "100943815_2682607968727505_8929927299822780416_n.jpg",
                                                    UserStore.BASE_PHOTO_API + "103579536_2677584742564247_5473764531253573593_o.jpg",
                                                    UserStore.BASE_PHOTO_API + "104369801_2679257449063643_3081244475482622855_n.jpg",
                                                    UserStore.BASE_PHOTO_API + "102322998_2665017303820991_469229819947646976_o.jpg",
                                                    UserStore.BASE_PHOTO_API + "84308057_1455728674615322_2743416309941198712_o.jpg"
                                                ]}
                                            />
                                        </div>
                                    </CardBody>
                                </div>
                            )}
                        </Card>
                        <Card small className="comments card-post card-post--aside card-post--1">
                            <CardHeader>
                                <h5 className="card-title">Comments ({UserStore.eventComments && UserStore.eventComments.total ? UserStore.eventComments.total.total : 0})</h5>                  
                            </CardHeader>
                            <CardBody>
                                <CommentInput></CommentInput>
                                {this.state.loading ? (
                                    <div className="loading-container">
                                        <Loader
                                            type="Oval"
                                            color="#1d84ef"
                                            height={50}
                                            width={50}
                                        />
                                    </div>
                                ) : (
                                    <div>
                                        {UserStore.eventComments.comments
                                        ? (
                                            <div>
                                                {UserStore.eventComments.comments && UserStore.eventComments.comments.map((list, idx) => (
                                                    <Col lg="12" md="12" sm="12" className="mb-3 d-inline-flex" key={idx}>
                                                        <img
                                                            className="user-comment-avatar rounded-circle mr-2"
                                                            src={UserStore.BASE_PHOTO_API + list.userAvatar}
                                                            alt="User Avatar"
                                                        />
                                                        <div className="comment-content-container">
                                                            <b>{list.userFullName}</b>
                                                            <div id="comment-content">{ReactHtmlParser(list.content)}</div>
                                                        </div>
                                                    </Col>
                                                ))}
                                            </div>
                                            )
                                            : null
                                        }
                                    </div>
                                )}
                                {this.state.loadingComments ? (
                                    <div className="loading-container">
                                        <Loader
                                            type="Oval"
                                            color="#1d84ef"
                                            height={50}
                                            width={50}
                                        />
                                    </div>
                                ) : (
                                    <div className="w-100 text-center">
                                        <button type="button" className="invisible mx-auto p-0 more-comments-button" onClick={(e) => this.showMoreComments(e)}>
                                            <h2 className="material-icons visible p-0 mb-0">keyboard_arrow_down</h2>
                                        </button>
                                    </div>
                                )}
                            </CardBody>
                        </Card>
                    </Col>
                </Row> 
            </div>
        </div>
      )
    }
}
export default withRouter(EventDetail);

