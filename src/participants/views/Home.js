import React from "react";
import EventsBlock from "../components/EventsBlock"
import {
    Form,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    FormInput
  } from "shards-react";
  
class Home extends React.Component {
    render() {
      return (
            <div className="home-page">
                <div className="search-filter-container">
                    <Form className="main-navbar__search w-100 d-md-flex d-lg-flex">
                        <InputGroup seamless className="ml-3">
                        <InputGroupAddon type="prepend">
                            <InputGroupText>
                            <i className="material-icons">search</i>
                            </InputGroupText>
                        </InputGroupAddon>
                        <FormInput
                            className="navbar-search"
                            placeholder="Search for something..."
                        />
                        </InputGroup>
                    </Form>
                </div>
                <EventsBlock></EventsBlock>
            </div>
      )
    }
}
export default Home;
