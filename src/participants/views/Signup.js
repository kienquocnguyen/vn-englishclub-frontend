import React from "react";
import axios from 'axios';
import {
  Row,
  Col,
  Form,
  Button,
  FormInput,
  FormCheckbox,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  InputGroupText
} from "shards-react";
//import css module
import ImageUploader from 'react-images-upload';
import {
	withRouter
} from 'react-router-dom';
import UserStore from '../../MobxStore/UserStore';


class Signup extends React.Component {
    constructor(props){
      super(props);
      this.selectedcountry = ''
      this.selectedregion = ''
      this.state = {
        country: '',
        city: '',
        firstName: '',
        lastName: '',
        username: '',
        password: '',
        facebook: '',
        email: '',
        phoneNumber: '',
        address: '',
        district: '',
        ward: '',
        gender: 'Male',
        avatar: [],
        unvalid: false,
        interneterror: false
      }
    }
    handleFirstname(text){
      this.setState({firstName: text.target.value})
    }
    handleLastname(text){
      this.setState({lastName: text.target.value})
    }
    handleUsername(text){
      this.setState({username: text.target.value})
    }
    handlePassword(text){
      this.setState({password: text.target.value})
    }
    handleFacebook(text){
      this.setState({facebook: text.target.value})
    }
    handleEmail(text){
      this.setState({email: text.target.value})
    }
    handlePhonenumber(text){
      this.setState({phoneNumber: text.target.value})
    }
    handleAddress(text){
      this.setState({address: text.target.value})
    }
    handleDistrict(text){
      this.setState({district: text.target.value})
    }
    handleWard(text){
      this.setState({ward: text.target.value})
    }
    
    selectCountry (val) {
      this.setState({ country: val });
      this.selectedcountry = val
      console.log(this.selectedcountry)
    }
    selectRegion (val) {
      this.setState({ region: val });
      this.selectedregion = val
      console.log(this.selectedregion)
    }
    SelectGender = (text) => {  
      this.setState({gender: text.target.value})
    }  
    onDrop = (picture) =>{
      this.setState({avatar: picture});
    }

    handleSubmit = async (event) =>{
      event.preventDefault();

      const user = new FormData();
      user.append("firstName", this.state.firstName);
      user.append("lastName", this.state.lastName);
      user.append("username", this.state.username);
      user.append("password", this.state.password);
      user.append("facebook", this.state.facebook);
      user.append("email", this.state.email);
      user.append("phoneNumber", this.state.phoneNumber);
      user.append("gender", this.state.gender);
      user.append("address", this.state.address);
      user.append("district", this.state.district);
      user.append("ward", this.state.ward);
      user.append("avatar", this.state.avatar[0]);
      const signupResponse = await UserStore.signup(user)
      if(signupResponse.status == 401){
          this.setState({unvalid: true})
          this.setState({interneterror: false})
      }else if(signupResponse.status == 200){
          if(signupResponse.role !== "admin"){
              this.props.history.push('/userprofile');
          }else{
              this.props.history.push('/blog-overview');
          }
          alert("Signup Success")
      }else{
          alert("Internet Error. Please check your connection")
      }
    }
    render() {
        const { country, region } = this.state;
        return (
            <div className="signup-page">
              <div className="row">
                <div className="col-xl-8 col-lg-10 signup-form">
                    <div className="mb-4 pt-3 card card-small">
                        <Col sm="12" md="12">
                        <h2 className="text-muted d-block mb-2">Sign Up</h2><br/>
                        <Form onSubmit={this.handleSubmit}>
                        <a className="text-muted d-block mb-2">Upload Avatar </a>
                        <FormGroup>
                        <ImageUploader
                            withIcon={true}
                            buttonText='Choose images'
                            onChange={this.onDrop}
                            imgExtension={['.jpg', '.gif', '.png', '.gif']}
                            singleImage={true}
                            withPreview={true}
                        />
                        </FormGroup>
                        <a className="text-muted d-block mb-2">First Name </a>
                        <FormGroup>
                            <FormInput
                                placeholder="First Name(required)"
                                //value=""
                                onChange={(text) => {this.handleFirstname(text)}}
                            />
                            </FormGroup>
                        <a className="text-muted d-block mb-2">Last Name </a>
                        <FormGroup>
                            <FormInput
                            placeholder="Last Name(required)"
                            //value=""
                            onChange={(text) => {this.handleLastname(text)}}
                            />
                        </FormGroup>
                        <a className="text-muted d-block mb-2">Gender </a>
                        <FormGroup>
                            <select id="feInputState"
                             className="form-control custom-select"
                             value={this.state.gender}
                             onChange={(text) => {this.SelectGender(text)}}
                             >
                              <option value="Male">Male</option>  
                              <option value="Female">Female</option>  
                              <option value="LGBT">LGBT</option>   
                            </select>
                        </FormGroup>
                        <a className="text-muted d-block mb-2">User Name </a>
                        <FormGroup>
                            <InputGroup className="mb-3">
                            <InputGroupAddon type="prepend">
                                <InputGroupText>
                                  <i className="material-icons">person</i>
                                </InputGroupText>
                            </InputGroupAddon>
                            <FormInput 
                            placeholder="Username(required)" 
                            onChange={(text) => {this.handleUsername(text)}}
                            />
                            </InputGroup>
                        </FormGroup>
                        <a className="text-muted d-block mb-2">Password </a>
                        <FormGroup>
                            <FormInput
                            type="password"
                            placeholder="Password"
                            //value="myCoolPassword"
                            onChange={(text) => {this.handlePassword(text)}}
                            />
                        </FormGroup>
                        <a className="text-muted d-block mb-2">Location </a>
                        <Row form>
                            <Col md="6" className="form-group country-select">
                            {/* Country and district Selector */}
                            </Col>
                        </Row>
                        
                        <FormGroup>
                            <FormInput
                            placeholder="Address(Optinal)"
                            //value=""
                            onChange={(text) => {this.handleAddress(text)}}
                            />
                        </FormGroup>
                        <FormGroup>
                            <FormInput
                            placeholder="District(Optinal)"
                            //value=""
                            onChange={(text) => {this.handleDistrict(text)}}
                            />
                            <div className = "valid-suggestions">Example: Queens District, District 4, Tan Binh District,...</div>
                        </FormGroup>
                        <FormGroup>
                            <FormInput
                            placeholder="Ward(Optinal)"
                            //value=""
                            onChange={(text) => {this.handleWard(text)}}
                            />
                        </FormGroup>
                        <a className="text-muted d-block mb-2">Phone Number </a>
                        <FormGroup>
                            <FormInput
                            placeholder="Phone Number(Optional)"
                            //value=""
                            onChange={(text) => {this.handlePhonenumber(text)}}
                            />
                        </FormGroup>
                        <a className="text-muted d-block mb-2">Facebook Link </a>
                        <FormGroup>
                            <FormInput
                            placeholder="Facebook Link(Optional)"
                            //value=""
                            onChange={(text) => {this.handleFacebook(text)}}
                            />
                        </FormGroup>
                        <a className="text-muted d-block mb-2">Email </a>
                        <FormGroup>
                            <FormInput
                            placeholder="Email (required)"
                            //value=""
                            onChange={(text) => {this.handleEmail(text)}}
                            />
                        </FormGroup>
                        <FormGroup>
                        <FormCheckbox defaultChecked>I agree with your <a href="#">Privacy Policy.</a></FormCheckbox>
                        </FormGroup>
                        <Button theme="primary" className="mb-2 mr-1">
                            Create New Account
                        </Button>
                        </Form>
                    </Col>
                    </div>
                </div>
              </div>
            </div>
      )
    }
}
export default withRouter(Signup);
