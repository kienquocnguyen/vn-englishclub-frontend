import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Button
} from "shards-react";
import {
	withRouter
} from 'react-router-dom';
import UserActions from "../MainNavbar/NavbarNav/UserActions";
import EventDetailSidebar from "./views/EventDetail/EventDetailSidebar";
import EvendDetailSidebarFooter from "./views/EventDetail/EvendDetailSidebarFooter";
import UserStore from "../../../MobxStore/UserStore";
import ClubStore from "../../../MobxStore/ClubStore";

class RightSidebar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      singleEventPage: false,
      menuVisible: false,
      openCalendar: true
    };

  }
  componentDidMount =  async () => {
    await UserStore.getUserInfo();
    //page function
    const currentRoute = window.location.pathname.split("/")
    const eventDetailPage = currentRoute[currentRoute.length - 2];
    if(eventDetailPage == "eventdetail"){
      await ClubStore.getClubDetail(UserStore.singleEvent.lgclub_id);
      this.setState({singleEventPage: true});
    }
    else{
      return;
    }
  }

  render() {
    const classes = classNames(
      "main-sidebar",
      "px-0",
      "col-12",
      "right-sidebar",
      this.state.menuVisible && "full-sidebar", "open"
    );
    return (
      <div>
        {this.state.openCalendar ? (
          <button className="show-right-sidebar-button" onClick={() => {this.setState({menuVisible: true, openCalendar: false})}}> 
            <i className="material-icons show-right-sidebar-icon">insert_invitation</i>
          </button>
        ) : (
          <button className="cancel-right-sidebar-button" onClick={() => {this.setState({menuVisible: false, openCalendar: true})}}> 
            <i className="material-icons show-right-sidebar-icon">close</i>
          </button>
        )} 
        <Col
          tag="aside"
          className={classes}
          lg={{ size: 2 }}
          md={{ size: 3 }}
        >
          <div className={this.state.clubDescriptionStyle ? "vh-height" : "autoheight"}>
            <UserActions />
            <div className="page-function mt-3">
              {this.state.singleEventPage
              ? (
                <EventDetailSidebar
                  totalParticipants={UserStore.singleEvent ? UserStore.singleEvent.totalParticipant : null}
                  totalLike={UserStore.singleEvent ? UserStore.singleEvent.totalLike : null}
                  clubLogo={ClubStore.clubDetail ? ClubStore.clubDetail.logo : null}
                  clubName={ClubStore.clubDetail ? ClubStore.clubDetail.name : null}
                  clubDescription={ClubStore.clubDetail ? ClubStore.clubDetail.description : null}
                />
                )
                : null
              }
            </div>
          </div>
          <div className="right-sidebar-footer mb-3 mt-2">
            {this.state.singleEventPage
            ? (
              <EvendDetailSidebarFooter
                address={UserStore.singleEvent ? UserStore.singleEvent.address : ''}
                ward={UserStore.singleEvent ? UserStore.singleEvent.ward : ''}
                district={UserStore.singleEvent ? UserStore.singleEvent.district : ''}
                city={UserStore.singleEvent ? UserStore.singleEvent.city : ''}
                country={UserStore.singleEvent ? UserStore.singleEvent.country : ''}
              />
              )
              : null
            }
          </div>
        </Col>
      </div>
    );
  }
}

RightSidebar.propTypes = {
  /**
   * Whether to hide the logo text, or not.
   */
  hideLogoText: PropTypes.bool
};

RightSidebar.defaultProps = {
  hideLogoText: false
};

export default withRouter(RightSidebar);
