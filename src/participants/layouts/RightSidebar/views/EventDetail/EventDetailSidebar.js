import React from "react";
import PropTypes from "prop-types";
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Button
} from "shards-react";
import UserStore from "../../../../../MobxStore/UserStore";
import ClubStore from "../../../../../MobxStore/ClubStore";

class EventDetailSidebar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clubDescriptionStyle: true,
      checkAttendEvent: false,
    };
  }

  attendEvent = async () =>{
    const body = {
      event_id: UserStore.singleEvent._id,
      event_title: UserStore.singleEvent.title
    }
    const data = await ClubStore.userAttendEvent(body);
    if(data){
      return alert("Thank you for signup this event");
    }else{
      return;
    }
  }
  componentDidMount = async () =>{
    const checkAttend = await ClubStore.checkUserAttendEvent(UserStore.singleEvent._id);
    if(checkAttend == true){
      this.setState({checkAttendEvent: false});
    }else if(checkAttend == false){
      this.setState({checkAttendEvent: true});
    }else{
      this.setState({checkAttendEvent: false});
    }
  }
  showFullDescription = () =>{
    this.setState({clubDescriptionStyle: !this.state.clubDescriptionStyle})
  }


  render() {
    return (
        <div className="event-detail-sidebar">
            <Row>
                <Col xs="11" className="mb-4">
                    <Card small className="card-post card-post--1">
                      <CardBody>
                          <Row>
                            <Col xs="6" className="text-center">
                                <p className="card-title d-inline-block mb-2 text-center">{this.props.totalParticipants}</p>
                                <h6 className="card-title text-fiord-blue text-center"> Participants </h6>
                            </Col>
                            <Col xs="6" className="text-center">
                                <p className="card-title d-inline-block mb-2 text-center">{this.props.totalLike}</p>
                                <h6 className="card-title text-fiord-blue text-center">Like</h6>
                            </Col>
                          </Row>
                          <Row className="pt-4">
                            <Col xs="12">
                              {this.state.checkAttendEvent ? (
                                <Button className="primary w-100 attend-event-button" onClick={this.attendEvent}>Attend This Event</Button>
                              ) : (
                                <Button className="w-100 cancel-event-button">Cancel</Button>
                              )}
                            </Col>
                          </Row>
                      </CardBody>
                    </Card>
                </Col>
            </Row>
            <Row className="organizer">
                <Col xs="11" className="mb-4">
                    <Card small className="card-post card-post--1">
                        <CardHeader>
                            <CardTitle>Organizer</CardTitle>
                        </CardHeader>
                        <CardBody>
                            <img
                                className="club-logo rounded-circle mr-2"
                                src={UserStore.BASE_PHOTO_API + this.props.clubLogo}
                                alt="Club Logo"
                            />
                            <h6 className="organizer-name d-md-inline-block">
                                {this.props.clubName}
                            </h6>
                            <div className={this.state.clubDescriptionStyle ? "limit-height-description mt-3 sidebar-club-description" : "max-height-description mt-3 sidebar-club-description"}>
                                {this.props.clubDescription}
                            </div>
                            <button onClick={this.showFullDescription} className="etc">... {this.state.clubDescriptionStyle ? "See More" : "Hide"}</button>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </div>
    );
  }
}

EventDetailSidebar.propTypes = {
  /**
   * Whether to hide the logo text, or not.
   */
  hideLogoText: PropTypes.bool
};

EventDetailSidebar.defaultProps = {
  hideLogoText: false
};

export default EventDetailSidebar;
