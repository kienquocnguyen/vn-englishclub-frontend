import React from "react";
import {
  Row,
  Col,
  Button
} from "shards-react";

class EvendDetailSidebarFooter extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      clubDescriptionStyle: true,
    };

  }

  render() {
    return (
        <Row className="right_sidebar_action">
            <Col xs="6">
            <a href={`http://maps.google.com/?q=${this.props.address}, ward ${this.props.ward}, district ${this.props.district}, ${this.props.city} city, ${this.props.country}`} target="_blank">
                <Button className="sidebar_checkmap_button">
                See On Map <p className="material-icons sidebar_checkmap_icon">location_on</p>
                </Button>
            </a>
            </Col>
            <Col xs="6">
                <Button className="sidebar_checkmap_button">
                Your Calendar <p className="material-icons sidebar_checkmap_icon">insert_invitation</p>
                </Button>
            </Col>
        </Row>
    );
  }
}

export default EvendDetailSidebarFooter;
