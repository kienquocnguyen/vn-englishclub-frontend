import React from "react";
import {
    FormInput,
    Form
  } from "shards-react";

import {
	withRouter
} from 'react-router-dom';
import UserStore from '../../MobxStore/UserStore';
import { observer } from "mobx-react";
import ReactQuill, { Quill } from "react-quill";
import quillEmoji from 'quill-emoji';
import "react-quill/dist/quill.snow.css";
import "quill-emoji/dist/quill-emoji.css";
import Resizer from 'react-image-file-resizer';

const Compress = require('compress.js');
const { EmojiBlot, ShortNameEmoji, ToolbarEmoji, TextAreaEmoji } = quillEmoji;

Quill.register({
  'formats/emoji': EmojiBlot,
  'modules/emoji-shortname': ShortNameEmoji,
  'modules/emoji-toolbar': ToolbarEmoji,
  'modules/emoji-textarea': TextAreaEmoji
}, true);
const toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () =>{ 
        resolve(reader.result)
    };
    reader.onerror = error => reject(error);
});
let commentImageFiles = [];
let commentPageLimt = 3;
@observer
class CommentInput extends React.Component {
    constructor(props){
        super(props);
        this.state={
            commentContent: '',
            openEmoji: false,
            userData: false,
            comments: []
        }
    }

    componentDidMount = () =>{
       
    }
    imageHandler = async () =>{
        const input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');
        input.click();
        input.onchange = async () => {
            if(commentImageFiles.length > 0){
                alert("Maximum 1 image for a comment")
            }else{
                const file = input.files[0];
                const base64 = await toBase64(file);
                this.setState({commentContent: this.state.commentContent + `<img src="${base64}"/>`})
                for(let i = 0; i < input.files.length; i++){
                    commentImageFiles.push(input.files[i])
                }
            }
        }
    }
    sendComment = async (e) =>{  
        e.preventDefault();
        const htmlContainer = document.createElement('div');
        htmlContainer.innerHTML = this.state.commentContent;
        if(commentImageFiles.length !== 0){
            const allImg = htmlContainer.getElementsByTagName('img');
            for(let i = 0; i < allImg.length; i++){
                var image = allImg[i];
                image.className = "comment-images"
                image.src = UserStore.BASE_PHOTO_API + commentImageFiles[i].name;
            }

            //Compress image
            const compress = new Compress();
            const resizedImage = await compress.compress([commentImageFiles[0]], {
                quality: 0.75, // the quality of the image, max is 1,
                maxWidth: 800, // the max width of the output image, defaults to 1920px
                resize: true // defaults to true, set false if you do not want to resize the image width and height
            })
            const img = resizedImage[0];
            const base64str = img.data
            const imgExt = img.ext
            const resizedFile = Compress.convertBase64ToFile(base64str, imgExt)
            resizedFile.name = commentImageFiles[0].name;

            //Add to formdata then upload
            const formData = new FormData();
            formData.append("commentPhoto", resizedFile, commentImageFiles[0].name)
            await UserStore.uploadCommentImages(formData);
        }

        //Submit the body comment
        const body = {
            event_id: UserStore.singleEvent._id,
            event_title: UserStore.singleEvent.title,
            type: "comment",
            content: htmlContainer.innerHTML
        }
        await UserStore.createComment(body);
        this.setState({commentContent: ""});
        await UserStore.getEventComments(UserStore.singleEvent._id, 1, commentPageLimt);
    }
    handleChange = (html) =>{
        this.setState({commentContent: html});
    }
    render() {
        const user = UserStore.userInfo;
        return (
            <div className="comment-input mb-4">
                <Form className="mb-4 d-inline-flex w-100">
                    <img
                        className="user-comment-avatar rounded-circle mr-2"
                        src={user && user.avatar ? UserStore.BASE_PHOTO_API + user.avatar : require("../../admin/images/avatars/0.jpg")}
                        alt="User Avatar"
                    />
                    <ReactQuill
                        id="rich-text-comment"
                        ref={(el) => this.quill = el}
                        className="rich-text-comment"
                        onChange={this.handleChange}
                        placeholder="Type your comment..."
                        value={this.state.commentContent}
                        modules={{
                            toolbar: {
                                container: [
                                    ['image'],
                                    ['emoji']
                                ],
                                handlers: {
                                    image: this.imageHandler
                                },
                            },
                            clipboard: {
                              // toggle to add extra line breaks when pasting HTML:
                              matchVisual: false,
                            },
                            'emoji-toolbar': true,
                            "emoji-textarea": true,
                            "emoji-shortname": true
                        }}
                        formats={CommentInput.formats}
                    >
                    </ReactQuill>
                </Form>
                <div className="send-container">
                    <button type="button" className="invisible mx-auto p-0" onClick={this.sendComment}>
                        <h4 className="material-icons p-0 mb-0 visible">send</h4>
                    </button>
                </div>
            </div>          
        )
    }
}

  CommentInput.formats = [
    'image',
    'emoji'
  ]
export default withRouter(CommentInput);

