import React from "react";
import {
    Row,
    Col,
    Card,
    CardBody,
    Badge
   
  } from "shards-react";
import CalendarIcon from "react-calendar-icon";
import UserStore from "../../MobxStore/UserStore";
import {
    Link,
	withRouter
} from 'react-router-dom';
import PaginationComponent from "react-reactstrap-pagination";
import { observer } from "mobx-react";

const dateOptions = {
    footer: { day: "2-digit" },
    value: { month: "short" },
    backgroundColor: "#f2f6f9",
    textColor: "#3f3f3f",
    locale: "eng"
};
@observer
class EventsBlock extends React.Component {
    constructor(props){
        super(props);
        this.state={
            eventList: [],
            selectedPage: 1,
            pageTotal: 1
        }
        this.handleSelected = this.handleSelected.bind(this);
    }
    
    handleSelected = async (selectedPage) => {
        this.setState({ selectedPage: selectedPage });
        UserStore.setEventPage(selectedPage, 3);
        const eventsResponse  = await UserStore.events();
        if(eventsResponse){
            this.setState({eventList: eventsResponse.event});
            return;
        }else{
            alert("Lost internet connection")
        }
    }
    
    componentDidMount =  async () => {
        UserStore.setEventPage(1, 3);
        const eventsResponse  = await UserStore.events();
        if(eventsResponse){
            // this.setState({eventList: eventsResponse.event});
            // this.setState({pageTotal: eventsResponse.total.total})
            return;
        }else{
            alert("Lost internet connection")
        }
    }
    render() {
        const eventList = UserStore.eventList.event;
        const eventTotal = UserStore.eventList.total;
        return (
            <div className="event-list">
                {UserStore.eventList && eventList
                    ? (
                    <Row>
                        {eventList && eventList.map((list, idx) => (
                            <Col lg="12" md="12" sm="12" className="mb-4" key={idx}>
                                <Link to={{
                                        pathname: "/eventdetail/" + list.slug
                                    }}                        
                                >
                                    <Card small className="card-post card-post--aside card-post--1">
                                        <CardBody>
                                            <Row>
                                                <Col lg="10" md="12">    
                                                    <h5 className="card-title">
                                                        <div className="calendar-icon"
                                                        >
                                                            <CalendarIcon date={new Date(list.startDate)} options={dateOptions}/>
                                                        </div>
                                                        <div className="event-info">
                                                            <a className="event-title" href="#" >
                                                                {list.title}
                                                            </a>
                                                            <div className="event-location">
                                                                <h6 className="material-icons">location_on</h6>
                                                                <span>{list.address}</span>
                                                            </div>
                                                        
                                                        </div>
                                                    </h5>
                                                </Col>
                                                <Col lg="2" md="12"> 
                                                    <div className="event-info">
                                                        <div className="event-category">
                                                            <Row>
                                                                <Col lg="6" md="2" >
                                                                    <Badge
                                                                        pill
                                                                        className={'card-post__category bg-info ' + list.type}  
                                                                    >
                                                                        {list.type}
                                                                    </Badge>
                                                                </Col>
                                                                <Col lg="6" md="2">
                                                                    <h4 className="material-icons booking-icon" style={{color:"#33C0FF"}}>bookmark_border</h4>
                                                                </Col>
                                                            </Row>
                                                        </div>
                                                    </div>
                                                </Col>
                                            </Row>
                                            <div className="card-post__image" style={{ backgroundImage: `url(${UserStore.BASE_PHOTO_API}${list.avatar})` }}>
                                            </div>
                                            <p className="card-text d-inline-block mb-4">{list.description}</p>
                                            <br/>
                                            <div className="event-list-footer">
                                                <Row>
                                                    <Col md="8" sm="8" className="event-post-author">
                                                    <div className="card-post__author d-flex"
                                                        >
                                                        <img
                                                            className="card-post__author-avatar card-post__author-avatar--small"
                                                            src={list && list.authorAvatar ? UserStore.BASE_PHOTO_API + list.authorAvatar : require("../../admin/images/avatars/0.jpg")}
                                                        />
                                                        <h5>+ {list.totalParticipant}</h5>
                                                    </div>
                                                    </Col>
                                                    <Col md="4" sm="4" className="event-counting">
                                                        <div className="event-count">
                                                            <Row>
                                                                <Col md="4" sm="4">
                                                                    <div className="event-bottom-view">
                                                                        <h4 className="material-icons" >comment</h4>
                                                                        <p className="event-counting-number">{list.totalComment}</p>
                                                                    </div>
                                                                </Col>
                                                                <Col md="4" sm="4">
                                                                    <div className="event-bottom-view">
                                                                        <h4 className="material-icons" >visibility</h4>
                                                                        <p className="event-counting-number">{list.totalViews}</p>
                                                                    </div>
                                                                </Col>
                                                                <Col md="4" sm="4">
                                                                    <div className="event-bottom-view">
                                                                        <h4 className="material-icons" >favorite_border</h4>
                                                                        <p className="event-counting-number">{list.totalLike}</p>
                                                                    </div>
                                                                </Col>
                                                            </Row>
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </div>
                                        </CardBody>
                                    </Card>
                                </Link>
                            </Col>
                        ))}
                    </Row>
                    )
                    : null
                }
                <PaginationComponent
                    className="pagination"
                    totalItems={eventTotal && eventTotal.total ? eventTotal.total : 0}
                    pageSize={UserStore.eventLimit}
                    onSelect={this.handleSelected}
                    defaultActivePage={1}
                />     
            </div>
        )
    }
}
export default withRouter(EventsBlock);
