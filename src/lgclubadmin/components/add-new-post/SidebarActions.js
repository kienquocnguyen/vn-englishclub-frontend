/* eslint jsx-a11y/anchor-is-valid: 0 */

import React from "react";
import PropTypes from "prop-types";
import {
  Card,
  CardHeader,
  CardBody,
  ListGroup,
  ListGroupItem,
  Button
} from "shards-react";
import { observer } from "mobx-react";
import UserStore from "../../../MobxStore/UserStore";

@observer
class SidebarActions extends React.Component {
  constructor(props) {
    super(props);
  }
  handleSubmit = async event => {
    event.preventDefault();
    const PostResponse = await UserStore.englishclubpostevent(
      UserStore.postEventBody
    );
    if (PostResponse.status == 401) {
      alert("You have to login");
    } else if (PostResponse.status == 200) {
      alert("Post Success");
    } else {
      alert(PostResponse.data.message);
    }
  };
  render() {
    return (
      <Card small className="mb-3">
        <CardHeader className="border-bottom">
          <h6 className="m-0">Actions</h6>
        </CardHeader>

        <CardBody className="p-0">
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <span className="d-flex mb-2">
                <i className="material-icons mr-1">flag</i>
                <strong className="mr-1">Status:</strong> Draft{" "}
                <a className="ml-auto" href="#">
                  Edit
                </a>
              </span>
              <span className="d-flex mb-2">
                <i className="material-icons mr-1">visibility</i>
                <strong className="mr-1">Visibility:</strong>{" "}
                <strong className="text-success">Public</strong>{" "}
                <a className="ml-auto" href="#">
                  Edit
                </a>
              </span>
              <span className="d-flex mb-2">
                <i className="material-icons mr-1">calendar_today</i>
                <strong className="mr-1">Schedule:</strong> Now{" "}
                <a className="ml-auto" href="#">
                  Edit
                </a>
              </span>
              <span className="d-flex">
                <i className="material-icons mr-1">score</i>
                <strong className="mr-1">Readability:</strong>{" "}
                <strong className="text-warning">Ok</strong>
              </span>
            </ListGroupItem>
            <ListGroupItem className="d-flex px-3 border-0">
              <Button outline theme="accent" size="sm">
                <i className="material-icons">save</i> Save Draft
              </Button>
              <Button
                theme="accent"
                size="sm"
                className="ml-auto"
                onClick={this.handleSubmit}
              >
                <i className="material-icons">file_copy</i> Publish
              </Button>
            </ListGroupItem>
          </ListGroup>
        </CardBody>
      </Card>
    );
  }
}

export default SidebarActions;
