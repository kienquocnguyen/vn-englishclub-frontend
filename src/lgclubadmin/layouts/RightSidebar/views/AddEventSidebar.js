import React from "react";
import { Row, Col } from "shards-react";
import SidebarActions from "../../../components/add-new-post/SidebarActions";
import SidebarCategories from "../../../components/add-new-post/SidebarCategories";


class AddEventSidebar extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
        <div className="add-event-sidebar">
            <Row>
                <Col xs="11" className="mb-4">
                    <SidebarActions />
                    <SidebarCategories />
                </Col>
            </Row>
        </div>
    );
  }
}

export default AddEventSidebar;
