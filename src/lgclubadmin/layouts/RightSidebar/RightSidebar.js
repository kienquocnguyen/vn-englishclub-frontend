import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Col } from "shards-react";
import UserActions from "../MainNavbar/NavbarNav/UserActions";
import UserStore from "../../../MobxStore/UserStore";
import AddEventSidebar from "./views/AddEventSidebar";

class RightSidebar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      menuVisible: false,
      openCalendar: true
    };

  }
  componentDidMount =  async () => {
    const loggedinResponse = await UserStore.getUserInfo();
    if(loggedinResponse){
        return;
    }else{
        return;
    }
}

  render() {
    const classes = classNames(
      "main-sidebar",
      "px-0",
      "col-12",
      "right-sidebar",
      this.state.menuVisible && "full-sidebar", "open"
    );
    return (
      <div>
        {this.state.openCalendar ? (
          <button className="show-right-sidebar-button" onClick={() => {this.setState({menuVisible: true, openCalendar: false})}}> 
            <i className="material-icons show-right-sidebar-icon">insert_invitation</i>
          </button>
        ) : (
          <button className="cancel-right-sidebar-button" onClick={() => {this.setState({menuVisible: false, openCalendar: true})}}> 
            <i className="material-icons show-right-sidebar-icon">close</i>
          </button>
        )} 
        <Col
          tag="aside"
          className={classes}
          lg={{ size: 2 }}
          md={{ size: 3 }}
        >
          <div className="autoheight">
            <UserActions />
            <div className="page-function mt-3">
              <AddEventSidebar></AddEventSidebar>
            </div>
          </div>
        </Col>
      </div>
    );
  }
}

RightSidebar.propTypes = {
  /**
   * Whether to hide the logo text, or not.
   */
  hideLogoText: PropTypes.bool
};

RightSidebar.defaultProps = {
  hideLogoText: false
};

export default RightSidebar;
