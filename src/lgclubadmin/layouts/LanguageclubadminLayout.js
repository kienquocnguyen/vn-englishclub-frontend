import React from "react";
import PropTypes from "prop-types";
import { Container, Row, Col } from "shards-react";

import MainNavbar from "./MainNavbar/MainNavbar";
import MainFooter from "./MainFooter";
import MainSidebar from "./MainSidebar/MainSidebar";
import RightSidebar from "./RightSidebar/RightSidebar";
import '../assets/style.css';
const LanguageclubadminLayout = ({ children, noNavbar, noFooter }) => (
  <Container fluid>
    <Row className = "language-club-layout">
      <MainSidebar />
      <Col
        className="main-content p-0"
        lg={{ size: 10, offset: 2 }}
        md={{ size: 9, offset: 3 }}
        sm="12"
        tag="main"
      >
        {!noNavbar && <MainNavbar />}
        {children}
        {!noFooter && <MainFooter />}
      </Col>
      <RightSidebar></RightSidebar>
    </Row>
  </Container>
);

LanguageclubadminLayout.propTypes = {
  /**
   * Whether to display the navbar, or not.
   */
  noNavbar: PropTypes.bool,
  /**
   * Whether to display the footer, or not.
   */
  noFooter: PropTypes.bool
};

LanguageclubadminLayout.defaultProps = {
  noNavbar: false,
  noFooter: false
};

export default LanguageclubadminLayout;
