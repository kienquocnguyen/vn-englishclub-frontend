export default function() {
  return [
    {
      title: "Home",
      to: "/language-club-home",
      htmlBefore: '<i class="material-icons">edit</i>',
      htmlAfter: ""
    },
    {
      title: "Signup",
      htmlBefore: '<i class="material-icons">assignment_turned_in</i>',
      to: "/language-club-signup"
    },
    {
      title: "Add New Event",
      htmlBefore: '<i class="material-icons">note_add</i>',
      to: "/language-club-add-new-event"
    }
  ];
}
