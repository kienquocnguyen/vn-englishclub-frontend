import React from "react";
import {
  Form,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  FormInput
} from "shards-react";

class LanguageclubHome extends React.Component {
  render() {
    return (
      <div className="home-page">
        <div className="search-filter-container">
          <Form className="main-navbar__search w-100 d-md-flex d-lg-flex">
            <InputGroup seamless className="ml-3">
              <InputGroupAddon type="prepend">
                <InputGroupText>
                  <i className="material-icons">search</i>
                </InputGroupText>
              </InputGroupAddon>
              <FormInput
                className="navbar-search"
                placeholder="Search for something..."
              />
            </InputGroup>
          </Form>
        </div>
        <h2>This Is English Club Home Page</h2>
      </div>
    );
  }
}
export default LanguageclubHome;
