import React from "react";
import {
  Container,
  Row,
  Col,
  FormGroup,
  Card,
  CardBody,
  Form,
  FormInput,
  Button
} from "shards-react";

import PageTitle from "../components/common/PageTitle";
//import Editor from "../components/add-new-post/Editor";
import ImageUploader from "react-images-upload";
import moment from "moment";
import UserStore from "../../MobxStore/UserStore";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import "../assets/quill.css";
import DateTimePicker from "react-datetime-picker";
import { observer } from "mobx-react";
import { observable } from "mobx";

@observer
class LanguageclubAddNewEvent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      authorld: "5ee2238ed8adea34744ab8ea",
      lgclub_id: "5ee239444dbbc13be46287c8",
      title: "",
      description: "",
      eventAvatar: [],
      type: "weekly", //weekly, daily, special
      address: "", //of the event
      city: "",
      ward: "",
      district: "",
      zipCode: "700000",
      country: "",
      day_off: new Date(), //if the type == daily
      startDate: new Date(),
      expiredDate: new Date(),
      date: new Date()
    };

    this.handleDescription = this.handleDescription.bind(this);
  }
  handleTitle(text) {
    this.setState({ title: text.target.value });
  }
  handleDescription(value) {
    this.setState({ description: value });
  }
  handleAddress(text) {
    this.setState({ address: text.target.value });
  }
  handleCity(text) {
    this.setState({ city: text.target.value });
  }
  handlecountry(text) {
    this.setState({ country: text.target.value });
  }
  handleWard(text) {
    this.setState({ ward: text.target.value });
  }
  handleDistrict(text) {
    this.setState({ district: text.target.value });
  }
  //Select || Onchange
  SelectType = text => {
    this.setState({ type: text.target.value });
  };
  onDropEventavatar = picture => {
    // this.setState({ eventAvatar: picture });
    console.log(picture);
    let startDateConvert = moment(new Date(this.state.startDate)).toISOString();
    let expiredDateConvert = moment(
      new Date(this.state.expiredDate)
    ).toISOString();

    const lgadmin = new FormData();
    lgadmin.append("authorId", UserStore.userInfo._id);
    lgadmin.append("lgclub_id", this.state.lgclub_id);
    lgadmin.append("title", this.state.title);
    lgadmin.append("description", this.state.description);
    lgadmin.append("address", this.state.address);
    lgadmin.append("city", this.state.city);
    lgadmin.append("type", this.state.type);
    lgadmin.append("ward", this.state.ward);
    lgadmin.append("district", this.state.district);
    lgadmin.append("country", this.state.country);
    lgadmin.append("startDate", startDateConvert);
    lgadmin.append("expiredDate", expiredDateConvert);
    lgadmin.append("avatar", picture[0]);
    UserStore.setEventBody(lgadmin);
  };
  PickStartdate = startDate => this.setState({ startDate });
  PickExpiredDate = expiredDate => this.setState({ expiredDate });

  handleSubmit = async event => {
    event.preventDefault();
    // let startDateConvert = moment(new Date(this.state.startDate)).toISOString();
    // let expiredDateConvert = moment(
    //   new Date(this.state.expiredDate)
    // ).toISOString();

    // const lgadmin = new FormData();
    // lgadmin.append("authorId", UserStore.userInfo._id);
    // lgadmin.append("lgclub_id", this.state.lgclub_id);
    // lgadmin.append("title", this.state.title);
    // lgadmin.append("description", this.state.description);
    // lgadmin.append("address", this.state.address);
    // lgadmin.append("city", this.state.city);
    // lgadmin.append("type", this.state.type);
    // lgadmin.append("ward", this.state.ward);
    // lgadmin.append("district", this.state.district);
    // lgadmin.append("country", this.state.country);
    // lgadmin.append("startDate", startDateConvert);
    // lgadmin.append("expiredDate", expiredDateConvert);
    // lgadmin.append("avatar", this.state.eventAvatar[0]);
    // UserStore.setEventBody(lgadmin);
    // const PostResponse = await UserStore.englishclubpostevent(lgadmin);
    // console.log(PostResponse);
    // if (PostResponse.status == 401) {
    //   alert("You have to login");
    // } else if (PostResponse.status == 200) {
    //   alert("Post Success");
    // } else {
    //   alert(PostResponse.data.message);
    // }
  };
  render() {
    return (
      <Container fluid className="main-content-container px-4 pb-4">
        {/* Page Header */}
        <Form onSubmit={this.handleSubmit}>
          <Row noGutters className="page-header py-4">
            <PageTitle
              sm="4"
              title="Add New Event"
              subtitle="Blog Posts"
              className="text-sm-left"
            />
          </Row>

          <div className="add-new-event">
            <Row>
              {/* Editor */}
              <Col lg="12" md="12">
                <Card small className="mb-3">
                  <CardBody>
                    <Form className="add-new-post">
                      <FormInput
                        size="lg"
                        className="mb-3"
                        placeholder="Your Post Title"
                        onChange={text => {
                          this.handleTitle(text);
                        }}
                      />
                      <ReactQuill
                        className="add-new-post__editor mb-1"
                        value={this.state.description}
                        onChange={this.handleDescription}
                      />
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
            <Card small className="mb-3">
              <CardBody>
                <div class="row">
                  <div class="col-sm-4">
                    <a className="text-muted d-block mb-2">
                      Select type of the event
                    </a>
                    <select
                      id="feInputState"
                      className="form-control custom-select"
                      value={this.state.type}
                      onChange={text => {
                        this.SelectType(text);
                      }}
                    >
                      <option value="daily">Daily</option>
                      <option value="weekly">Weekly</option>
                      <option value="special">Special</option>
                    </select>
                  </div>
                  <div class="col-sm-4">
                    <a className="text-muted d-block mb-2">
                      Select the start date
                    </a>
                    <DateTimePicker
                      format={"dd-MM-yyyy hh:mm"}
                      onChange={this.PickStartdate}
                      value={this.state.startDate}
                    />
                  </div>
                  <div class="col-sm-4">
                    <a className="text-muted d-block mb-2">
                      Select the expired date
                    </a>
                    <DateTimePicker
                      format={"dd-MM-yyyy hh:mm"}
                      onChange={this.PickExpiredDate}
                      value={this.state.expiredDate}
                    />
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <a className="text-muted d-block mb-2">Address: </a>
                    <FormInput
                      size="lg"
                      className="mb-3"
                      onChange={text => {
                        this.handleAddress(text);
                      }}
                    />
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <a className="text-muted d-block mb-2">Ward: </a>
                    <FormInput
                      size="lg"
                      className="mb-3"
                      onChange={text => {
                        this.handleWard(text);
                      }}
                    />
                  </div>
                  <div class="col-sm-6">
                    <a className="text-muted d-block mb-2">District: </a>
                    <FormInput
                      size="lg"
                      className="mb-3"
                      onChange={text => {
                        this.handleDistrict(text);
                      }}
                    />
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <a className="text-muted d-block mb-2">City: </a>
                    <FormInput
                      size="lg"
                      className="mb-3"
                      onChange={text => {
                        this.handleCity(text);
                      }}
                    />
                  </div>
                  <div class="col-sm-6">
                    <a className="text-muted d-block mb-2">Country: </a>
                    <FormInput
                      size="lg"
                      className="mb-3"
                      onChange={text => {
                        this.handlecountry(text);
                      }}
                    />
                  </div>
                </div>
                <div className="mb-4 pt-3 card card-small">
                  <Col sm="12" md="12">
                    <a className="text-muted d-block mb-2">
                      Upload the event background picture:{" "}
                    </a>
                    <FormGroup>
                      <ImageUploader
                        withIcon={true}
                        buttonText="Choose images"
                        onChange={this.onDropEventavatar}
                        imgExtension={[".jpg", ".gif", ".png", ".gif"]}
                        singleImage={true}
                        withPreview={true}
                      />
                    </FormGroup>
                  </Col>
                </div>
              </CardBody>
            </Card>
          </div>
        </Form>
      </Container>
    );
  }
}

export default LanguageclubAddNewEvent;
