import React from "react";
import { Redirect } from "react-router-dom";

// Layout Types
import { DefaultLayout } from "./admin/layouts";
import { ParticipantsLayout } from "./participants/layouts";
import LanguageclubadminLayout from "./lgclubadmin/layouts/LanguageclubadminLayout";

// Route Views Admin Page
import BlogOverview from "./admin/views/BlogOverview";
import UserProfileLite from "./admin/views/UserProfileLite";
import AddNewPost from "./admin/views/AddNewPost";
import Errors from "./admin/views/Errors";
import ComponentsOverview from "./admin/views/ComponentsOverview";
import Tables from "./admin/views/Tables";
import BlogPosts from "./admin/views/BlogPosts";

//Route Views
import Home from "./participants/views/Home";
import Login from "./participants/views/Login";
import Signup from "./participants/views/Signup";
import UserProfile from "./participants/views/UserProfile";
import EventDetail from "./participants/views/EventDetail";

//Route Views Language Club Admin
import LanguageclubSignup from "./lgclubadmin/views/LanguageclubSignup";
import LanguageclubHome from "./lgclubadmin/views/LanguageclubHome";
import LanguageclubAddNewEvent from "./lgclubadmin/views/LanguageclubAddNewEvent";

export default [
  {
    path: "/",
    exact: true,
    layout: ParticipantsLayout,
    component: () => <Redirect to="/home" />
  },
  {
    path: "/home",
    layout: ParticipantsLayout,
    component: Home
  },
  {
    path: "/login",
    layout: ParticipantsLayout,
    component: Login
  },
  {
    path: "/signup",
    layout: ParticipantsLayout,
    component: Signup
  },
  {
    path: "/userprofile",
    layout: ParticipantsLayout,
    component: UserProfile
  },
  {
    path: "/eventdetail/:permalink",
    layout: ParticipantsLayout,
    component: EventDetail
  },
  {
    path: "/blog-overview",
    layout: DefaultLayout,
    component: BlogOverview
  },
  {
    path: "/user-profile-lite",
    layout: DefaultLayout,
    component: UserProfileLite
  },
  {
    path: "/add-new-post",
    layout: DefaultLayout,
    component: AddNewPost
  },
  {
    path: "/errors",
    layout: DefaultLayout,
    component: Errors
  },
  {
    path: "/components-overview",
    layout: DefaultLayout,
    component: ComponentsOverview
  },
  {
    path: "/tables",
    layout: DefaultLayout,
    component: Tables
  },
  {
    path: "/blog-posts",
    layout: DefaultLayout,
    component: BlogPosts
  },
  {
    path: "/language-club-signup",
    layout: LanguageclubadminLayout,
    component: LanguageclubSignup
  },
  {
    path: "/language-club-home",
    layout: LanguageclubadminLayout,
    component: LanguageclubHome
  },
  {
    path: "/language-club-add-new-event",
    layout: LanguageclubadminLayout,
    component: LanguageclubAddNewEvent
  }
];
