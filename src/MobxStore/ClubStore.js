import { observable, action, set, toJS } from 'mobx';
import API from './api/index';

class Club {
    @observable.ref clubDetail = {};

    @action getClubDetail = async (id) =>{
        try{
            const response = await API.clubDetail(id);
            if(response){
                if(response.status == 200){
                    this.clubDetail = response.data;
                    return response.data;
                }else{
                    alert(response.data.message)
                    return;
                }
            }
            else{
                alert("Lost internet connection");
                return;
            }
        }catch(error){
            console.log(error)
        }
    }
    @action userAttendEvent = async (body) =>{
        try{
            const response = await API.attendEvent(body);
            if(response){
                if(response.status == 200){
                    return response.data;
                }else{
                    alert(response.data.message)
                    return;
                }
            }
            else{
                alert("Lost internet connection");
                return;
            }
        }catch(error){
            console.log(error)
        }
    }
    @action checkUserAttendEvent = async (id) =>{
        try{
            const response = await API.checkAttendedEvent(id);
            if(response){
                if(response.status == 200){
                    return response.data.checkParticipated;
                }else if (response.status == 401){
                    return;
                }else{
                    alert(response.data.message)
                    return;
                }
            }else{
                alert("Lost internet connetcion");
                return;
            }
        }catch(error){
            console.log(error)
        }
    }
}

const ClubStore = new Club();
export default ClubStore;