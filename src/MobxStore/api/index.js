import { post, get, put, del } from "./baseAPI";
import UserStore from "../UserStore";
const BASE_API_DEV = "http://localhost:3000/";

const BaseURL = BASE_API_DEV;

export default class ServerApi {
  //POST API
  static login = body => {
    const url = BaseURL + "users/login";
    const header = {
      "Content-Type": "application/json"
    };
    return post(url, body, header);
  };
  static signup = body => {
    const url = BaseURL + "signup/participants";
    const header = {
      "Content-Type": "multipart/form-data"
    };
    return post(url, body, header);
  };
  static englishclubsignup = body => {
    const url = BaseURL + "signup/lgclubadmin";
    const header = {
      "Content-Type": "multipart/form-data"
    };
    return post(url, body, header);
  };
  static englishclubpostevent = body => {
    const url = BaseURL + "events";
    const header = {
      "Content-Type": "multipart/form-data",
      Authorization: "Bearer" + " " + localStorage.getItem("cool-jwt")
    };
    return post(url, body, header);
  };
  //PUT API
  static userprofile = body => {
    const url = BaseURL + "users/update";
    console.log(localStorage.getItem("cool-jwt"));
    const header = {
      "Content-Type": "application/json",
      Authorization: "Bearer" + " " + localStorage.getItem("cool-jwt")
    };
    return put(url, body, header);
  };
  //GET API
  static logedin = () => {
    const url = BaseURL + "users/loggedin";
    const header = {
      "Content-Type": "application/json",
      Authorization: "Bearer" + " " + localStorage.getItem("cool-jwt")
    };
    return get(url, header);
  };
  static events = () => {
    const params = {
      page: UserStore.eventPage,
      limit: UserStore.eventLimit
    };
    const url = BaseURL + "events";
    const header = {
      "Content-Type": "application/json"
    };
    return get(url, header, params);
  };
  static eventDetail = permalink => {
    const url = BaseURL + "eventdetail/" + permalink;
    const header = {
      "Content-Type": "application/json"
    };
    return get(url, header);
  };
  static eventComment = (id, page, limit) => {
    const params = {
      eventId: id,
      page: page,
      limit: limit
    };
    const url = BaseURL + "event/eventComments";
    const header = {
      "Content-Type": "application/json"
    };
    return get(url, header, params);
  };
  static newComment = body => {
    const url = BaseURL + `eventComments`;
    const header = {
      "Content-Type": "application/json",
      Authorization: "Bearer" + " " + UserStore.userToken
    };
    return post(url, body, header);
  };
  static uploadCommentPhotos = body => {
    const url = BaseURL + `eventComments/uploadPhoto`;
    const header = {
      "Content-Type": "application/json",
      Authorization: "Bearer" + " " + UserStore.userToken
    };
    return post(url, body, header);
  };
  static eventComment = (id, page, limit) =>{
        const params = {
            eventId: id,
            page: page,
            limit: limit
        }
        const url = BaseURL + 'event/eventComments';
        const header = {
            'Content-Type': 'application/json'
        };
        return get(url, header, params);
    }
    static newComment = (body) =>{
        const url = BaseURL + `eventComments`;
        const header = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + ' ' + UserStore.userToken
        };
        return post(url, body, header);
    }
    static uploadCommentPhotos = (body) =>{
        const url = BaseURL + `eventComments/uploadPhoto`;
        const header = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + ' ' + UserStore.userToken
        };
        return post(url, body, header);
    }
    static clubDetail = (id) =>{
        const url = BaseURL + 'languageclub/' + id;
        const header = {
            'Content-Type': 'application/json',
        };
        return get(url, header);
    }
    static likeEvent = (body) =>{
        const url = BaseURL + `eventLike`;
        const header = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + ' ' + UserStore.userToken
        };
        return post(url, body, header);
    }
    static checkLikeEvent = (id) =>{
        const url = BaseURL + `eventLike/checkLiked/` + id;
        const header = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + ' ' + localStorage.getItem("cool-jwt")
        };
        return get(url, header);
    }
    static deleteLikeEvent = (event_id, user_id) =>{
        const params = {
            eventId: event_id,
            userId: user_id
        }
        const url = BaseURL + `eventLike/delete`;
        const header = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + ' ' + UserStore.userToken
        };
        return del(url, header, params);
    }
    static attendEvent = (body) =>{
        const url = BaseURL + 'eventParticipation';
        const header = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + ' ' + localStorage.getItem("cool-jwt")
        };
        return post(url, body, header);
    }
    static checkAttendedEvent = (id) =>{
        const url = BaseURL + `eventParticipation/checkParticipated/` + id;
        const header = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + ' ' + localStorage.getItem("cool-jwt")
        };
        return get(url, header);
    }
}