import { observable, action, set, toJS } from "mobx";
import API from "./api/index";

class User {
  @observable firstName = "Quoc";
  @observable userToken = "";
  @observable eventPage = 1;
  @observable eventLimit = 3;
  @observable BASE_PHOTO_API = "http://localhost:3000/uploads/";
  @observable.ref eventList = [];
  @observable.ref eventComments = [];
  @observable.ref userInfo = {};
  @observable.ref singleEvent = {};
  @observable.ref postEventBody = {};

  @action setEventPage = (page, litmit) => {
    this.eventPage = page;
    this.eventLimit = litmit;
  };
  @action showRightSidebar = sidebar => {
    this.RightSidebar = sidebar;
  };
  @action login = async body => {
    try {
      const response = await API.login(body);
      if (response.status === 200) {
        localStorage.setItem("cool-jwt", response.data.token);
        const user = await API.logedin();
        this.userInfo = user.data;
      } else {
        console.log("login failed");
      }
      return response;
    } catch (error) {
      console.log(error);
    }
  };
  @action userprofile = async body => {
    try {
      const response = await API.userprofile(body);
      return response;
    } catch (error) {
      console.log(error);
    }
  };
  @action signup = async body => {
    try {
      const response = await API.signup(body);
      return response;
    } catch (error) {
      console.log(error);
    }
  };
  @action englishclubsignup = async body => {
    try {
      console.log(body);
      const response = await API.englishclubsignup(body);
      console.log(response);
      return response;
    } catch (error) {
      console.log(error);
    }
  };
  @action logedin = async () => {
    try {
      const response = await API.logedin();
      if (response) {
        if (response.status === 200) {
          this.userToken = localStorage.getItem("cool-jwt");
          this.userInfo = response.data;
          return response;
        } else {
          if (response.status === 401) {
            alert("Please login first");
          }
          return;
        }
      } else {
        alert("Lost internet connection");
        return;
      }
    } catch (error) {
      console.log(error);
    }
  };
  @action getUserInfo = async () => {
    try {
      const response = await API.logedin();
      if (response) {
        if (response.status === 200) {
          this.userToken = localStorage.getItem("cool-jwt");
          this.userInfo = response.data;
          return response;
        } else {
          return response;
        }
      } else {
        alert("Lost internet connection");
        return;
      }
    } catch (error) {
      console.log(error);
    }
  };
  @action logout = async () => {
    try {
      localStorage.removeItem("cool-jwt");
      this.userToken = "";
    } catch (error) {
      console.log("ERROR" + error);
    }
  };
  @action events = async () => {
    try {
      const response = await API.events();
      if (response) {
        if (response.status === 200) {
          this.eventList = response.data;
          return response.data;
        }
      } else {
        return;
      }
      return response;
    } catch (error) {
      console.log(error);
    }
  };
  @action getEventDetail = async permalink => {
    try {
      const response = await API.eventDetail(permalink);
      if (response) {
        if (response.status === 200) {
          this.singleEvent = response.data[0];
          return response.data;
        } else {
          alert(response.data.message);
          return;
        }
      } else {
        alert("Lost internet connection");
        return;
      }
    } catch (error) {
      console.log(error);
    }
  };

  @action getEventComments = async (id, page, limit) => {
    try {
      const response = await API.eventComment(id, page, limit);
      if (response) {
        if (response.status === 200) {
          this.eventComments = response.data;
          return response.data;
        } else if (response.status === 404) {
          this.eventComments = [];
          return;
        } else {
          alert(response.data.message);
          return;
        }
      } else {
        alert("Lost internet connection");
        return;
      }
    } catch (error) {
      console.log(error);
    }
  };

  @action createComment = async (body) =>{
    try{
        const response = await API.newComment(body);
        if(response){
            if(response.status == 200){
                return response.data;
            }else{
                alert(response.data.message)
                return;
            }
        }
        else{
            alert("Lost internet connection");
            return;
        }
    }catch(error){
        console.log(error)
    }
  }

  @action uploadCommentImages = async body => {
    try {
      const response = await API.uploadCommentPhotos(body);
      console.log(response);
      if (response) {
        if (response.status === 200) {
          return response.data;
        }else {
          alert(response.data);
          return;
        }
      }else {
        alert("Lost internet connection");
        return;
      }
    } catch (error) {
      console.log(error);
    }
  };
  @action checkLikedEvent = async (id) =>{
    try{
        const response = await API.checkLikeEvent(id);
        if(response){
            if(response.status == 200){
                this.likeEventChecked = response.data.checkLiked;
                return response.data;
            }else if (response.status == 401){
                return;
            }else{
                alert(response.data.message)
                return;
            }
        }else{
            alert("Lost internet connetcion");
            return;
        }
    }catch(error){
        console.log(error)
    }
  }
  @action likeEvent = async (body) =>{
    try{
      const response = await API.likeEvent(body);
      if(response){
          if(response.status == 200){
              return response.data;
          }else if (response.status == 401){
              return;
          }else{
              alert(response.data.message)
              return;
          }
      }else{
          alert("Lost internet connetcion");
          return;
      }
    }catch(error){
        console.log(error)
    }
  } 
  @action unLikeEvent = async (event_id, user_id) =>{
    try{
      const response = await API.deleteLikeEvent(event_id, user_id);
      if(response){
          if(response.status == 200){
              return response.data;
          }else if (response.status == 401){
              return;
          }else{
              alert(response.data.message)
              return;
          }
      }else{
          alert("Lost internet connetcion");
          return;
      }
    }catch(error){
        console.log(error)
    }
  } 
  @action englishclubpostevent = async (body) => {
    try {
      const response = await API.englishclubpostevent(body);
      if(response){
        if(response.status == 200){
          this.lgclubadmin = response.data;
          return response;
        }else{
          alert(response.data.message)
        }
      }else{
        alert("Lost internet connetcion");
        return;
      }
    } catch (error) {
      console.log(error);
    }
  };
  @action setEventBody = (body) => {
    this.postEventBody = body;
  };
}

const UserStore = new User();
export default UserStore;